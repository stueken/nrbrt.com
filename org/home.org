#+hugo_base_dir: ../
#+hugo_front_matter_format: yaml
#+author: Norbert Stüken
#+language: en

* About
** About
:properties:
:export_hugo_section: /
:export_file_name: about
:export_options: author:nil
:export_date: nil
:end:

*** This Site
*nrbrt.com* [fn:nrbrtcomsource] is created with *[[https://gohugo.io/][Hugo]]* and is based on the *[[https://github.com/adityatelange/hugo-PaperMod/][PaperMod]]* theme. All contents are written in *[[https://www.gnu.org/software/emacs/][GNU Emacs]] [[https://orgmode.org/][Org Mode]]* and converted to Hugo-compatible /[[https://en.wikipedia.org/wiki/Markdown][Markdown]]/ with the *[[https://ox-hugo.scripter.co/][ox-hugo]]* package. The Org Mode compatability and ox-hugo were the main reason for using Hugo as /[[https://indieweb.org/static_site_generator][static site generator]]/.

*** Whoami
- I am a *[[https://www.python.org/][Python]]* backend engineer working on *[[https://www.djangoproject.com/][Django]]* web applications with a secondary focus on [[https://en.wikipedia.org/wiki/DevOps][DevOps]] topics in software development environments.
- I use *[[https://neovim.io/][Neovim]]* [fn:neovimdotfiles] as [[https://en.wikipedia.org/wiki/Integrated_development_environment][IDE]] for coding, but otherwise rely more and more on *[[https://www.gnu.org/software/emacs/][GNU Emacs]]* [fn:emacsdotfiles] to make my life supposedly easier. 
- And yes, I have a strong passion for *GNU/Linux* [fn:linux] and prefer using *[[https://en.wikipedia.org/wiki/Free_and_open-source_software][Free/Libre and Open Source Software (FLOSS)]]* over products from [[https://en.wikipedia.org/wiki/Big_Tech][Big Tech]] companies like /Alphabet/ (/Google/), /Amazon/, /Apple/, /Meta/ (/Facebook/, /Instagram/, /WhatsApp/, ...) and /Microsoft/ [fn:mobile].
- Besides all the nerd stuff, I run in the *[[https://www.leichtathletik-berlin.de/berliner-laeufercup-47.html][Berliner Läufercup]]* series [fn:läufercup] and follow political news and discussions with my quite left-wing mindset [fn:newspapers].
- Since I am damn privileged in this world, I donate 10% of my monthly net income [fn:effectivealtruism]. I can only recommend everyone to do this in a similar way.
- I live and [[https://www.jonasundderwolf.de/][work]] in my hometown Berlin.

[fn:nrbrtcomsource] Find the source code for nrbrt.com [[https://gitlab.com/stueken/nrbrt.com][here]].
[fn:neovimdotfiles] Find my Neovim dotfiles [[https://github.com/stueken/dotfiles/tree/master/.config/nvim][here]].
[fn:emacsdotfiles] Find my Emacs dotfiles [[https://github.com/stueken/dotfiles/tree/master/.emacs.d][here]].
[fn:linux] I'm mostly using *[[https://www.debian.org][Debian]]*, but my work laptop currently still has *[[https://ubuntu.com/][Ubuntu]]* installed.
[fn:mobile] Since Linux is not (yet) really suitable for everyday use on mobile devices, I use the /Google/-free Android operationg systems *[[https://calyxos.org/][CalysOS]]* and *[[https://e.foundation][/e/OS]]*  am very satisfied with them.
[fn:läufercup] If you live in Berlin and you like running (at any level), then join our *BRLN RNNRS* team. Just [[contact page][contact me]] for more infos.
[fn:newspapers] For a broader, more analytical view on politics, I recommend the monthly magazine *[[https://www.blaetter.de/][Blätter für deutsche und internationale Politik]]* or the weekly newspaper *[[https://www.freitag.de/][der Freitag]]*. In addition, the weekly podcast *[[https://lagedernation.org/][Lage der Nation]]* takes a comprehensive look at daily political events and the political TV magazine *[[https://www1.wdr.de/daserste/monitor/][Monitor]]* offers investigative insights into selected topics.
[fn:effectivealtruism] Besides donations to political parties and organisations such as *[[https://www.medico.de/en/][medico international]]*, the *[[https://fsfe.org/index.en.html][Free Software Foundation Europe (FSFE)]]* and the *[[https://humanismus.de][Humanistischer Verband Deutschland (HVD)]]*, the majority of donations goes to organisations that demonstrably achieve particularly good results with their restricted funds (*[[https://www.effectivealtruism.org/][Effective Altruism]]*).
For a short introduction to effective altruism, watch [[https://www.ardmediathek.de/video/odysso-wissen-im-swr/wofuer-spenden-und-warum/swr/Y3JpZDovL3N3ci5kZS9hZXgvbzE1Nzg0NjY][this short video]] [German] from the TV magazine /odysso - Wissen im SWR/. For more information, I recommend the book /[[https://www.williammacaskill.com/book][Doing Good Better]]/ by William MacAskill and the German donation website [[https://www.effektiv-spenden.org/][effektiv-spenden.org]].

** Über
:properties:
:export_hugo_section: /
:export_file_name: about.de.md
:export_language: de
:export_options: author:nil
:end:

*** Diese Website
*nrbrt.com* [fn:nrbrtcomsource] wird mit *[[https://gohugo.io/][Hugo]]* erstellt und basiert auf dem *[[https://github.com/adityatelange/hugo-PaperMod/][PaperMod]]*-Theme. Die Inhalte werden im *[[https://www.gnu.org/software/emacs/][GNU Emacs]] [[https://orgmode.org/][Org Mode]]* geschrieben und mit dem Paket *[[https://ox-hugo.scripter.co/][ox-hugo]]* in Hugo-kompatibles /[[https://de.wikipedia.org/wiki/Markdown][Markdown]]/ konvertiert. Die Org Mode-Kompatibiliät und das Paket ox-hugo waren mit der Hauptgrund für Hugo als /[[https://indieweb.org/static_site_generator][Static Site Generator]]/.

*** Wer ich bin
- Ich bin *[[https://www.python.org/][Python]]*-Backend-Entwickler und arbeite an *[[https://www.djangoproject.com/][Django]]*-Webanwendungen, beschäftige mich aber auch mit *[[https://de.wikipedia.org/wiki/DevOps][DevOps]]*-Themen im Umfeld der Software-Entwicklung.
- Zum Programmieren nutze ich *[[https://neovim.io/][Neovim]]* [fn:neovimdotfiles] als [[https://de.wikipedia.org/wiki/Integrierte_Entwicklungsumgebung][IDE]], verlasse mich aber immer mehr auf *[[https://www.gnu.org/software/emacs/][GNU Emacs]]* [fn:emacsdotfiles], um mein Leben vermeintlich einfacher zu gestalten.
- Und ja, ich habe eine Leidenschaft für *GNU/Linux* [fn:linux] und nutze lieber *[[https://de.wikipedia.org/wiki/Free/Libre_Open_Source_Software][Free/Libre and Open Source Software (FLOSS)]]* als Produkte von [[https://de.wikipedia.org/wiki/Big_Tech][Big Tech]]-Firmen wie /Alphabet/ (/Google/), /Amazon/, /Apple/, /Meta/ (/Facebook/, /Instagram/, /WhatsApp/, ...) und /Microsoft/ [fn:mobile].
- Neben all dem Nerd-Kram laufe ich beim *[[https://www.leichtathletik-berlin.de/berliner-laeufercup-47.html][Berliner Läufercup]]* mit [fn:läufercup] und interessiere mich für Politik mit einer recht linken Einstellung  [fn:newspapers].
- Und da ich in dieser Welt verdammt priviligiert bin, spende ich 10% meines monatlichen Nettoeinkommens [fn:effectivealtruism]. Ich kann nur jedem empfehlen, dieses auf ähnliche Weise auch zu tun.
- Ich lebe und [[https://www.jonasundderwolf.de/][arbeite]] in meiner Heimatstadt Berlin.

[fn:nrbrtcomsource] Den Quellcode von nrbrt.com findest Du [[https://gitlab.com/stueken/nrbrt.com][hier]].
[fn:neovimdotfiles] Meine Neovim-dotfiles findest Du [[https://github.com/stueken/dotfiles/tree/master/.config/nvim][hier]].
[fn:emacsdotfiles] Meine Emacs-dotfiles findest Du [[https://github.com/stueken/dotfiles/tree/master/.emacs.d][hier]].
[fn:linux] Ich nutze vor allem *[[https://www.debian.org][Debian]]*, auf meinem Arbeitslaptop ist zur Zeit aber noch *[[https://ubuntu.com/][Ubuntu]]* installiert.
[fn:mobile] Da Linux auf Mobilgeräten für mich (noch) nicht wirklich alltagstauglich ist, nutze ich dort die /Google/-freien Android-Betriebssysteme *[[https://calyxos.org/][CalysOS]]* und *[[https://e.foundation/de/][/e/OS]]* und bin damit sehr zufrieden. 
[fn:läufercup] Wenn Du in Berlin wohnst und gerne läufst (egal auf welchem Niveau), dann mach mit bei unserem *BRLN RNNRS*-Team. [[contact page][Kontaktiere mich]] einfach für mehr Infos.
[fn:newspapers] Für einen breiteren, analytischeren Blick auf politische Themen empfehle ich die Monatszeitschrift *[[https://www.blaetter.de/][Blätter für deutsche und internationale Politik]]* oder die Wochenzeitung *[[https://www.freitag.de/][der Freitag]]*. Darüber hinaus wirft der wöchentliche Podcast *[[https://lagedernation.org/][Lage der Nation]]* einen umfassenden Blick auf das politische Tagesgeschehen und das politische TV-Magazin *[[https://www1.wdr.de/daserste/monitor/][Monitor]]* bietet investigative Einblicke in ausgewählte Themen.
[fn:effectivealtruism] Neben Spenden an politische Parteien und Organisationen wie *[[https://www.medico.de/][medico international]]*, der *[[https://fsfe.org/index.de.html][Free Software Foundation Europe (FSFE)]]* und *[[https://humanismus.de][Humanistischer Verband Deutschland (HVD)]]*, geht der Großteil der Spenden an Organisationen, die mit Ihren zweckgebundenen Mitteln nachweislich besonders gute Ergebnisse erzielen (*[[https://www.effektiveraltruismus.de/][Effektiver Altruismus]]*).
Eine kurze Einführung in den effektiven Altruismus bietet [[https://www.ardmediathek.de/video/odysso-wissen-im-swr/wofuer-spenden-und-warum/swr/Y3JpZDovL3N3ci5kZS9hZXgvbzE1Nzg0NjY][dieses kurze Video]] aus dem TV-Magazin /odysso - Wissen im SWR/. Für weitere Informationen empfehle ich das Buch: /[[https://www.williammacaskill.com/book][Doing Good Better]]/ (deutscher Titel: /[[https://www.ullstein-buchverlage.de/nc/buch/details/gutes-besser-tun-9783843713399.html][Gutes besser tun]]/) von William MacAskill und die Spendenseite [[https://www.effektiv-spenden.org/][effektiv-spenden.org]].

* Contact
** Contact <<contact page>>
:properties:
:export_hugo_section: /
:export_file_name: contact
:export_options: author:nil
:end:

*** Email
You are welcome to write to me personally if you don't just want to market your service.

- Email :: @@hugo:{{< cloakemail address="norbert.stueken@nrbrt.com"  query="subject=Message via nrbrt.com">}}@@
- PGP Public Key [fn:pgp] :: [Download](https://files.nrbrt.com/norbert_stueken_pgp_public.asc)
- Fingerprint :: 2272 F886 7929 DAC9 2526 \\
                              34CE E7FE 0612 2BC7 45DC

*** Messenger
 You can reach me via the following messengers [fn:messengers].

- Signal :: If you have my mobile number
- Threema :: @@hugo:{{<cloakemail address="//threema.id/7HC52F67?text=" display="7HC52F67" protocol="https" >}}@@
- Matrix :: @@hugo:{{<cloakemail address="//matrix.to/#/@nrbrt:matrix.org" display="@nrbrt:matrix.org" protocol="https">}}@@
- Telegram :: @@hugo:{{< cloakemail address="//t.me/stueken?text=" display="stueken" protocol="https" >}}@@

*** Social Media
I try to get rid of most of my social media accounts [fn:socialmedia], but once in a while I toot on

#+header: :trim-post t
#+begin_term  # with non breakable space "\ nbsp{}"
- Mastodon [fn:fediverse] :: \nbsp{}
#+end_term
#+attr_html: :rel me
[[https://mastodon.social/@nrbrt][@nrbrt@mastodon.social]]

[fn:pgp] If you want to write encrypted emails but don't know what to do with terms like /[[https://en.wikipedia.org/wiki/Pretty_Good_Privacy][PGP]]/ and /Fingerprint/, then try the /[[https://emailselfdefense.fsf.org][Email Self-Defense Guide]]/ from the /Free Software Foundation (FSF)/.
[fn:messengers] A good orientation for choosing messengers is the regularly updated /[[https://www.messenger-matrix.de/messenger-matrix-en.html][Messenger-Matrix]]/ from Mike Kuketz. *[[https://signal.org][Signal]]* and *[[https://threema.ch/en][Threema]]* are recommended for everyone and the *[[https://matrix.org/][Matrix]]* protocol could be the future of messaging (read more about it [[https://www.republik.ch/2021/02/24/kill-the-messenger][here]] [German]). *[[https://telegram.org/][Telegram]]* is not recommended and per default unencrypted, but its client is /open source/ and offers more comfort than SMS. 
[fn:socialmedia] The film /[[https://www.thesocialdilemma.com/][The Social Dilemma]]/ shows very clearly what the consequences of our growing dependence on social media are.
[fn:fediverse] *[[https://joinmastodon.org/][Mastodon]]* is a /[[https://en.wikipedia.org/wiki/Microblogging][microblogging]]/ platform similar to /Twitter/, but decentralized and part of a greater network of platforms, the *Fediverse*. A good introduction can be found [[https://www.kuketz-blog.de/das-fediverse-unendliche-weiten-als-schaubild-diagramm/][here]] [German] on the /Kuketz-Blog/.

** Kontakt <<contact page>>
:properties:
:export_hugo_section: /
:export_file_name: contact.de.md
:export_language: de
:export_options: author:nil
:end:

*** E-Mail
Du kannst mich gerne persönlich anschreiben, vorausgesetzt Du willst mir nicht nur Deine Dienstleistung verkaufen.

- E-Mail :: @@hugo:{{< cloakemail address="norbert.stueken@nrbrt.com"  query="subject=Nachricht über nrbrt.com">}}@@
- PGP Public Key [fn:pgp] :: [Download](https://files.nrbrt.com/norbert_stueken_pgp_public.asc)
- Fingerprint :: 2272 F886 7929 DAC9 2526 \\
                              34CE E7FE 0612 2BC7 45DC

*** Messenger
 Du kannst mich über die folgenden Messenger erreichen [fn:messengers].

- Signal :: Wenn Du meine Handynummer hast
- Threema :: @@hugo:{{<cloakemail address="//threema.id/7HC52F67?text=" display="7HC52F67" protocol="https" >}}@@
- Matrix :: @@hugo:{{<cloakemail address="//matrix.to/#/@nrbrt:matrix.org" display="@nrbrt:matrix.org" protocol="https">}}@@
- Telegram :: @@hugo:{{< cloakemail address="//t.me/stueken?text=" display="stueken" protocol="https" >}}@@

*** Social Media
Ich versuche, die meisten meiner Konten zu löschen [fn:socialmedia], aber ab und zu /tröte/ ich auf

#+header: :trim-post t
#+begin_term  # with non breakable space "\ nbsp{}"
- Mastodon [fn:fediverse] :: \nbsp{}
#+end_term
#+attr_html: :rel me
[[https://mastodon.social/@nrbrt][@nrbrt@mastodon.social]]

[fn:pgp] Wenn Du mir verschlüsselte E-Mails schreiben möchtest, aber nicht weißt, was Du mit Begriffen wie /[[https://de.wikipedia.org/wiki/Pretty_Good_Privacy][PGP]]/ und /Fingerprint/ anfangen sollst, dann probiere mal den /[[https://emailselfdefense.fsf.org][Email Self-Defense Guide]]/ von der /Free Software Foundation (FSF)/ aus.
[fn:messengers] Eine gute Orientierung zur Auswahl von Messengern ist die regelmäßig aktualisierte /[[https://www.messenger-matrix.de/][Messenger-Matrix]]/ von Mike Kuketz. *[[https://signal.org/de/][Signal]]* und *[[https://threema.ch/de][Threema]]* sind für jeden zu empfehlen und das *[[https://matrix.org/][Matrix]]*-Protokoll könnte die Zukunft des Messaging sein (lies mehr dazu [[https://www.republik.ch/2021/02/24/kill-the-messenger][hier]]). *[[https://telegram.org/][Telegram]]* wird nicht empfohlen und ist standardmäßig unverschlüsselt, der Client ist aber /Open Source/ und bietet mehr Komfort als der Austausch über SMS.
[fn:socialmedia] Der Film /[[https://www.thesocialdilemma.com/de/][The Social Dilemma]]/ (deutscher Titel: /Das Dilemma mit den sozialen Medien/) zeigt sehr deutlich, welche Folgen unsere wachsende Abhängigkeit von sozialen Medien hat.
[fn:fediverse] *[[https://joinmastodon.org/][Mastodon]]* ist eine /[[https://de.wikipedia.org/wiki/Mikroblogging][Mikroblogging]]/-Plattform ähnlich wie /Twitter/, aber dezentralisiert und Teil eines größeren Netzwerks von Plattformen, dem *Fediverse*. Eine gute Einführung findet sich [[https://www.kuketz-blog.de/das-fediverse-unendliche-weiten-als-schaubild-diagramm/][hier]] auf dem /Kuketz-Blog/.

* Links
** Links
:properties:
:export_hugo_section: /
:export_file_name: links
:export_options: author:nil
:end:

- [[https://blog.nrbrt.com/][blog.nrbrt.com]] [fn:1]  :: A travel blog I've created with *[[https://blog.getpelican.com/][Pelican]]*. Just [[contact page][ask me]] for the credentials.
- [[https://schenkwerk.de/][schenkwerk.de]] :: Just a simple reminder site of the company Timo and I created between 2013 and 2016. *Schenkwerk* generated curated gift recommendations with the help of some machine learning. Still want to bring it back online some day ... 

[fn:1] Find the source code for blog.nrbrt.com [[https://github.com/stueken/blog][here]].

* Links - DE
** Links
:properties:
:export_hugo_section: /
:export_file_name: links.de.md
:export_language: de
:export_options: author:nil
:end:

- [[https://blog.nrbrt.com/][blog.nrbrt.com]] [fn:1]  :: Ein Reiseblog, den ich mit *[[https://blog.getpelican.com/][Pelican]]* erstellt habe. [[contact page][Frag mich]] einfach nach den Anmeldedaten.
- [[https://schenkwerk.de/][schenkwerk.de]] :: Nur eine einfache Erinnerungseite an das Unternehmen, das Timo und ich zwischen 2013 und 2016 gegründet haben. *Schenkwerk* generierte kuratierte Geschenkempfehlungen mit Hilfe von maschinellem Lernen. Irgendwann möchte ich das nochmal wieder online bringen ... 

[fn:1] Den Quellcode von blog.nrbrt.com findest Du [[https://github.com/stueken/blog][hier]].

* Posts
