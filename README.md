

# How to Generate this Site

1.  Clone this repository:
    
        git clone git@gitlab.com:stueken/nrbrt.com.git

2.  Change into the directory:
    
        cd nrbrt.com

3.  Update git submodules with:
    
        git submodule update --init --recursive

4.  Install Hugo, e.g. on Debian and Ubuntu with:
    
        sudo apt install hugo

5.  Start the Hugo dev server which runs the page at <http://localhost:1313/> with:
    
        hugo server -D


# How to Update the Markdown Content in GNU Emacs Org Mode


## Initially

Add the [ox-hugo](https://ox-hugo.scripter.co/doc/installation/) package to your GNU Emacs configuration


## Update Website Contents

1.  Edit the contents in `org/home.org`
2.  Export the contents to Hugo compatible Markdown with `C-x C-e H A`
3.  Run the Hugo dev server with `hugo server -D` and validate the output at <http://localhost:1313>
4.  Build the site with `hugo` and deploy the `public` folder to your server


## Update the `README.md`

1.  Edit the contents in `org/README.org`
2.  Export the contents to Markdown with `C-x C-e m m`
3.  Open the generated `README.md` file to verify the output

