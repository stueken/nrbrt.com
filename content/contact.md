---
title: "Contact "
draft: false
---

## Email {#email}

You are welcome to write to me personally if you don't just want to market your service.

Email
: {{< cloakemail address="norbert.stueken@nrbrt.com"  query="subject=Message via nrbrt.com">}}

PGP Public Key [^fn:1]
: [Download](<https://files.nrbrt.com/norbert_stueken_pgp_public.asc>)

Fingerprint
: 2272 F886 7929 DAC9 2526 <br />
    34CE E7FE 0612 2BC7 45DC


## Messenger {#messenger}

You can reach me via the following messengers&nbsp;[^fn:2].

Signal
: If you have my mobile number

Threema
: {{<cloakemail address="//threema.id/7HC52F67?text=" display="7HC52F67" protocol="https" >}}

Matrix
: {{<cloakemail address="//matrix.to/#/@nrbrt:matrix.org" display="@nrbrt:matrix.org" protocol="https">}}

Telegram
: {{< cloakemail address="//t.me/stueken?text=" display="stueken" protocol="https" >}}


## Social Media {#social-media}

I try to get rid of most of my social media accounts&nbsp;[^fn:3], but once in a while I toot on

<span class="term">Mastodon [^fn:4]
: &nbsp;</span> <a href="https://mastodon.social/@nrbrt" rel="me">@nrbrt@mastodon.social</a>

[^fn:1]: If you want to write encrypted emails but don't know what to do with terms like _[PGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy)_ and _Fingerprint_, then try the _[Email Self-Defense Guide](https://emailselfdefense.fsf.org)_ from the _Free Software Foundation (FSF)_.
[^fn:2]: A good orientation for choosing messengers is the regularly updated _[Messenger-Matrix](https://www.messenger-matrix.de/messenger-matrix-en.html)_ from Mike Kuketz. **[Signal](https://signal.org)** and **[Threema](https://threema.ch/en)** are recommended for everyone and the **[Matrix](https://matrix.org/)** protocol could be the future of messaging (read more about it [here](https://www.republik.ch/2021/02/24/kill-the-messenger) [German]). **[Telegram](https://telegram.org/)** is not recommended and per default unencrypted, but its client is _open source_ and offers more comfort than SMS.
[^fn:3]: The film _[The Social Dilemma](https://www.thesocialdilemma.com/)_ shows very clearly what the consequences of our growing dependence on social media are.
[^fn:4]: **[Mastodon](https://joinmastodon.org/)** is a _[microblogging](https://en.wikipedia.org/wiki/Microblogging)_ platform similar to _Twitter_, but decentralized and part of a greater network of platforms, the **Fediverse**. A good introduction can be found [here](https://www.kuketz-blog.de/das-fediverse-unendliche-weiten-als-schaubild-diagramm/) [German] on the _Kuketz-Blog_.