---
title: "Kontakt "
draft: false
---

## E-Mail {#e-mail}

Du kannst mich gerne persönlich anschreiben, vorausgesetzt Du willst mir nicht nur Deine Dienstleistung verkaufen.

E-Mail
: {{< cloakemail address="norbert.stueken@nrbrt.com"  query="subject=Nachricht über nrbrt.com">}}

PGP Public Key [^fn:1]
: [Download](<https://files.nrbrt.com/norbert_stueken_pgp_public.asc>)

Fingerprint
: 2272 F886 7929 DAC9 2526 <br />
    34CE E7FE 0612 2BC7 45DC


## Messenger {#messenger}

Du kannst mich über die folgenden Messenger erreichen&nbsp;[^fn:2].

Signal
: Wenn Du meine Handynummer hast

Threema
: {{<cloakemail address="//threema.id/7HC52F67?text=" display="7HC52F67" protocol="https" >}}

Matrix
: {{<cloakemail address="//matrix.to/#/@nrbrt:matrix.org" display="@nrbrt:matrix.org" protocol="https">}}

Telegram
: {{< cloakemail address="//t.me/stueken?text=" display="stueken" protocol="https" >}}


## Social Media {#social-media}

Ich versuche, die meisten meiner Konten zu löschen&nbsp;[^fn:3], aber ab und zu _tröte_ ich auf

<span class="term">Mastodon [^fn:4]
: &nbsp;</span> <a href="https://mastodon.social/@nrbrt" rel="me">@nrbrt@mastodon.social</a>

[^fn:1]: Wenn Du mir verschlüsselte E-Mails schreiben möchtest, aber nicht weißt, was Du mit Begriffen wie _[PGP](https://de.wikipedia.org/wiki/Pretty_Good_Privacy)_ und _Fingerprint_ anfangen sollst, dann probiere mal den _[Email Self-Defense Guide](https://emailselfdefense.fsf.org)_ von der _Free Software Foundation (FSF)_ aus.
[^fn:2]: Eine gute Orientierung zur Auswahl von Messengern ist die regelmäßig aktualisierte _[Messenger-Matrix](https://www.messenger-matrix.de/)_ von Mike Kuketz. **[Signal](https://signal.org/de/)** und **[Threema](https://threema.ch/de)** sind für jeden zu empfehlen und das **[Matrix](https://matrix.org/)**-Protokoll könnte die Zukunft des Messaging sein (lies mehr dazu [hier](https://www.republik.ch/2021/02/24/kill-the-messenger)). **[Telegram](https://telegram.org/)** wird nicht empfohlen und ist standardmäßig unverschlüsselt, der Client ist aber _Open Source_ und bietet mehr Komfort als der Austausch über SMS.
[^fn:3]: Der Film _[The Social Dilemma](https://www.thesocialdilemma.com/de/)_ (deutscher Titel: _Das Dilemma mit den sozialen Medien_) zeigt sehr deutlich, welche Folgen unsere wachsende Abhängigkeit von sozialen Medien hat.
[^fn:4]: **[Mastodon](https://joinmastodon.org/)** ist eine _[Mikroblogging](https://de.wikipedia.org/wiki/Mikroblogging)_-Plattform ähnlich wie _Twitter_, aber dezentralisiert und Teil eines größeren Netzwerks von Plattformen, dem **Fediverse**. Eine gute Einführung findet sich [hier](https://www.kuketz-blog.de/das-fediverse-unendliche-weiten-als-schaubild-diagramm/) auf dem _Kuketz-Blog_.