---
title: "Über"
draft: false
---

## Diese Website {#diese-website}

**nrbrt.com**&nbsp;[^fn:1] wird mit **[Hugo](https://gohugo.io/)** erstellt und basiert auf dem **[PaperMod](https://github.com/adityatelange/hugo-PaperMod/)**-Theme. Die Inhalte werden im **[GNU Emacs](https://www.gnu.org/software/emacs/) [Org Mode](https://orgmode.org/)** geschrieben und mit dem Paket **[ox-hugo](https://ox-hugo.scripter.co/)** in Hugo-kompatibles _[Markdown](https://de.wikipedia.org/wiki/Markdown)_ konvertiert. Die Org Mode-Kompatibiliät und das Paket ox-hugo waren mit der Hauptgrund für Hugo als _[Static Site Generator](https://indieweb.org/static_site_generator)_.


## Wer ich bin {#wer-ich-bin}

-   Ich bin **[Python](https://www.python.org/)**-Backend-Entwickler und arbeite an **[Django](https://www.djangoproject.com/)**-Webanwendungen, beschäftige mich aber auch mit **[DevOps](https://de.wikipedia.org/wiki/DevOps)**-Themen im Umfeld der Software-Entwicklung.
-   Zum Programmieren nutze ich **[Neovim](https://neovim.io/)**&nbsp;[^fn:2] als [IDE](https://de.wikipedia.org/wiki/Integrierte_Entwicklungsumgebung), verlasse mich aber immer mehr auf **[GNU Emacs](https://www.gnu.org/software/emacs/)**&nbsp;[^fn:3], um mein Leben vermeintlich einfacher zu gestalten.
-   Und ja, ich habe eine Leidenschaft für **GNU/Linux**&nbsp;[^fn:4] und nutze lieber **[Free/Libre and Open Source Software (FLOSS)](https://de.wikipedia.org/wiki/Free/Libre_Open_Source_Software)** als Produkte von [Big Tech](https://de.wikipedia.org/wiki/Big_Tech)-Firmen wie _Alphabet_ (_Google_), _Amazon_, _Apple_, _Meta_ (_Facebook_, _Instagram_, _WhatsApp_, ...) und _Microsoft_&nbsp;[^fn:5].
-   Neben all dem Nerd-Kram laufe ich beim **[Berliner Läufercup](https://www.leichtathletik-berlin.de/berliner-laeufercup-47.html)** mit&nbsp;[^fn:6] und interessiere mich für Politik mit einer recht linken Einstellung&nbsp;[^fn:7].
-   Und da ich in dieser Welt verdammt priviligiert bin, spende ich 10% meines monatlichen Nettoeinkommens&nbsp;[^fn:8]. Ich kann nur jedem empfehlen, dieses auf ähnliche Weise auch zu tun.
-   Ich lebe und [arbeite](https://www.jonasundderwolf.de/) in meiner Heimatstadt Berlin.

[^fn:1]: Den Quellcode von nrbrt.com findest Du [hier](https://gitlab.com/stueken/nrbrt.com).
[^fn:2]: Meine Neovim-dotfiles findest Du [hier](https://github.com/stueken/dotfiles/tree/master/.config/nvim).
[^fn:3]: Meine Emacs-dotfiles findest Du [hier](https://github.com/stueken/dotfiles/tree/master/.emacs.d).
[^fn:4]: Ich nutze vor allem **[Debian](https://www.debian.org)**, auf meinem Arbeitslaptop ist zur Zeit aber noch **[Ubuntu](https://ubuntu.com/)** installiert.
[^fn:5]: Da Linux auf Mobilgeräten für mich (noch) nicht wirklich alltagstauglich ist, nutze ich dort die _Google_-freien Android-Betriebssysteme **[CalysOS](https://calyxos.org/)** und **[/e/OS](https://e.foundation/de/)** und bin damit sehr zufrieden.
[^fn:6]: Wenn Du in Berlin wohnst und gerne läufst (egal auf welchem Niveau), dann mach mit bei unserem **BRLN RNNRS**-Team. [Kontaktiere mich]({{< relref "contact" >}}) einfach für mehr Infos.
[^fn:7]: Für einen breiteren, analytischeren Blick auf politische Themen empfehle ich die Monatszeitschrift **[Blätter für deutsche und internationale Politik](https://www.blaetter.de/)** oder die Wochenzeitung **[der Freitag](https://www.freitag.de/)**. Darüber hinaus wirft der wöchentliche Podcast **[Lage der Nation](https://lagedernation.org/)** einen umfassenden Blick auf das politische Tagesgeschehen und das politische TV-Magazin **[Monitor](https://www1.wdr.de/daserste/monitor/)** bietet investigative Einblicke in ausgewählte Themen.
[^fn:8]: Neben Spenden an politische Parteien und Organisationen wie **[medico international](https://www.medico.de/)**, der **[Free Software Foundation Europe (FSFE)](https://fsfe.org/index.de.html)** und **[Humanistischer Verband Deutschland (HVD)](https://humanismus.de)**, geht der Großteil der Spenden an Organisationen, die mit Ihren zweckgebundenen Mitteln nachweislich besonders gute Ergebnisse erzielen (**[Effektiver Altruismus](https://www.effektiveraltruismus.de/)**).
    Eine kurze Einführung in den effektiven Altruismus bietet [dieses kurze Video](https://www.ardmediathek.de/video/odysso-wissen-im-swr/wofuer-spenden-und-warum/swr/Y3JpZDovL3N3ci5kZS9hZXgvbzE1Nzg0NjY) aus dem TV-Magazin _odysso - Wissen im SWR_. Für weitere Informationen empfehle ich das Buch: _[Doing Good Better](https://www.williammacaskill.com/book)_ (deutscher Titel: _[Gutes besser tun](https://www.ullstein-buchverlage.de/nc/buch/details/gutes-besser-tun-9783843713399.html)_) von William MacAskill und die Spendenseite [effektiv-spenden.org](https://www.effektiv-spenden.org/).