---
title: "Links"
draft: false
---

[blog.nrbrt.com](https://blog.nrbrt.com/) [^fn:1]
: A travel blog I've created with **[Pelican](https://blog.getpelican.com/)**. Just [ask me]({{< relref "contact" >}}) for the credentials.

[schenkwerk.de](https://schenkwerk.de/)
: Just a simple reminder site of the company Timo and I created between 2013 and 2016. **Schenkwerk** generated curated gift recommendations with the help of some machine learning. Still want to bring it back online some day ...

[^fn:1]: Find the source code for blog.nrbrt.com [here](https://github.com/stueken/blog).