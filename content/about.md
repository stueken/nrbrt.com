---
title: "About"
draft: false
---

## This Site {#this-site}

**nrbrt.com**&nbsp;[^fn:1] is created with **[Hugo](https://gohugo.io/)** and is based on the **[PaperMod](https://github.com/adityatelange/hugo-PaperMod/)** theme. All contents are written in **[GNU Emacs](https://www.gnu.org/software/emacs/) [Org Mode](https://orgmode.org/)** and converted to Hugo-compatible _[Markdown](https://en.wikipedia.org/wiki/Markdown)_ with the **[ox-hugo](https://ox-hugo.scripter.co/)** package. The Org Mode compatability and ox-hugo were the main reason for using Hugo as _[static site generator](https://indieweb.org/static_site_generator)_.


## Whoami {#whoami}

-   I am a **[Python](https://www.python.org/)** backend engineer working on **[Django](https://www.djangoproject.com/)** web applications with a secondary focus on [DevOps](https://en.wikipedia.org/wiki/DevOps) topics in software development environments.
-   I use **[Neovim](https://neovim.io/)**&nbsp;[^fn:2] as [IDE](https://en.wikipedia.org/wiki/Integrated_development_environment) for coding, but otherwise rely more and more on **[GNU Emacs](https://www.gnu.org/software/emacs/)**&nbsp;[^fn:3] to make my life supposedly easier.
-   And yes, I have a strong passion for **GNU/Linux**&nbsp;[^fn:4] and prefer using **[Free/Libre and Open Source Software (FLOSS)](https://en.wikipedia.org/wiki/Free_and_open-source_software)** over products from [Big Tech](https://en.wikipedia.org/wiki/Big_Tech) companies like _Alphabet_ (_Google_), _Amazon_, _Apple_, _Meta_ (_Facebook_, _Instagram_, _WhatsApp_, ...) and _Microsoft_&nbsp;[^fn:5].
-   Besides all the nerd stuff, I run in the **[Berliner Läufercup](https://www.leichtathletik-berlin.de/berliner-laeufercup-47.html)** series&nbsp;[^fn:6] and follow political news and discussions with my quite left-wing mindset&nbsp;[^fn:7].
-   Since I am damn privileged in this world, I donate 10% of my monthly net income&nbsp;[^fn:8]. I can only recommend everyone to do this in a similar way.
-   I live and [work](https://www.jonasundderwolf.de/) in my hometown Berlin.

[^fn:1]: Find the source code for nrbrt.com [here](https://gitlab.com/stueken/nrbrt.com).
[^fn:2]: Find my Neovim dotfiles [here](https://github.com/stueken/dotfiles/tree/master/.config/nvim).
[^fn:3]: Find my Emacs dotfiles [here](https://github.com/stueken/dotfiles/tree/master/.emacs.d).
[^fn:4]: I'm mostly using **[Debian](https://www.debian.org)**, but my work laptop currently still has **[Ubuntu](https://ubuntu.com/)** installed.
[^fn:5]: Since Linux is not (yet) really suitable for everyday use on mobile devices, I use the _Google_-free Android operationg systems **[CalysOS](https://calyxos.org/)** and **[/e/OS](https://e.foundation)**  am very satisfied with them.
[^fn:6]: If you live in Berlin and you like running (at any level), then join our **BRLN RNNRS** team. Just [contact me]({{< relref "contact" >}}) for more infos.
[^fn:7]: For a broader, more analytical view on politics, I recommend the monthly magazine **[Blätter für deutsche und internationale Politik](https://www.blaetter.de/)** or the weekly newspaper **[der Freitag](https://www.freitag.de/)**. In addition, the weekly podcast **[Lage der Nation](https://lagedernation.org/)** takes a comprehensive look at daily political events and the political TV magazine **[Monitor](https://www1.wdr.de/daserste/monitor/)** offers investigative insights into selected topics.
[^fn:8]: Besides donations to political parties and organisations such as **[medico international](https://www.medico.de/en/)**, the **[Free Software Foundation Europe (FSFE)](https://fsfe.org/index.en.html)** and the **[Humanistischer Verband Deutschland (HVD)](https://humanismus.de)**, the majority of donations goes to organisations that demonstrably achieve particularly good results with their restricted funds (**[Effective Altruism](https://www.effectivealtruism.org/)**).
    For a short introduction to effective altruism, watch [this short video](https://www.ardmediathek.de/video/odysso-wissen-im-swr/wofuer-spenden-und-warum/swr/Y3JpZDovL3N3ci5kZS9hZXgvbzE1Nzg0NjY) [German] from the TV magazine _odysso - Wissen im SWR_. For more information, I recommend the book _[Doing Good Better](https://www.williammacaskill.com/book)_ by William MacAskill and the German donation website [effektiv-spenden.org](https://www.effektiv-spenden.org/).