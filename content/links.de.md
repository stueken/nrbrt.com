---
title: "Links"
draft: false
---

[blog.nrbrt.com](https://blog.nrbrt.com/) [^fn:1]
: Ein Reiseblog, den ich mit **[Pelican](https://blog.getpelican.com/)** erstellt habe. [Frag mich]({{< relref "contact" >}}) einfach nach den Anmeldedaten.

[schenkwerk.de](https://schenkwerk.de/)
: Nur eine einfache Erinnerungseite an das Unternehmen, das Timo und ich zwischen 2013 und 2016 gegründet haben. **Schenkwerk** generierte kuratierte Geschenkempfehlungen mit Hilfe von maschinellem Lernen. Irgendwann möchte ich das nochmal wieder online bringen ...

[^fn:1]: Den Quellcode von blog.nrbrt.com findest Du [hier](https://github.com/stueken/blog).